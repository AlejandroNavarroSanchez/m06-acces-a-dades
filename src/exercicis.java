import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class exercicis {
    // Rutes
    private final static Path projectPath = Path.of(System.getProperty("user.dir"));
    private final static Path srcPath = projectPath.resolve("src");
    private final static Path fitxersPath = srcPath.resolve("fitxers");
    private final static Path entersPath = fitxersPath.resolve("enters.txt");
    private final static Path restaurantsPath = fitxersPath.resolve("restaurants.csv");
    private final static Path actPath = srcPath.resolve("exercicis");

    // Scanner
    private final static Scanner lector = new Scanner(System.in).useLocale(Locale.US);
    private static final String NEW_LINE = System.lineSeparator();

    // 1.- Llistar el nom de tots els fitxers d'un directori que li passem per teclat.
    private static void ex1() {

        System.out.println("Introdueix una ruta: ");
        String input = lector.nextLine();
        File fitxers = new File(input);

        File[] listOfFiles = fitxers.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile())
                System.out.println((i+1)+" "+listOfFiles[i].getName());
        }
    }

    // 2.- Omplir un fitxer creat de nou amb els textos que li passes per teclat.
    private static void ex2() throws IOException {

        System.out.println("Ruta on vols crear l'arxiu: ");
        String ruta = lector.nextLine();

        Path path = Paths.get(ruta);

        System.out.println("Introdueix text: ");
        String text = lector.nextLine() + NEW_LINE;

        Files.writeString(path, text/*,
                StandardOpenOption.CREATE,
                StandardOpenOption.APPEND*/);
    }

    // 3.- Afegir el que escrius per teclat al final d'un fitxer de text ja existent.
    private static void ex3() throws IOException {
        System.out.println("Ruta on vols crear l'arxiu: ");
        String ruta = lector.nextLine();

        Path path = Paths.get(ruta);

        System.out.println("Introdueix text: ");
        String text = lector.nextLine() + NEW_LINE;

        Files.writeString(path, text,
                StandardOpenOption.CREATE,
                StandardOpenOption.APPEND);
    }

    // 4.- Llegir línia a línia el contingut d'un fitxer de text i mostrar-lo per pantalla.
    private static void ex4() {
        try {

            System.out.println("Introdueix la ruta a l'arxiu a llegir: ");
            String ruta = lector.nextLine();

            File f = new File(ruta);

            BufferedReader b = new BufferedReader(new FileReader(f));

            String readLine = "";

            while ((readLine = b.readLine()) != null) {
                System.out.println(readLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 5.- Llegir un fitxer línia a línia utilitzant la clase Scanner.
    private static void ex5() {
        try {
            System.out.println("Introdueix la ruta a l'arxiu a llegir: ");
            String nomArxiu = lector.nextLine();

            File arxiu = new File(nomArxiu);
            Scanner scanner = new Scanner(arxiu);

            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
            scanner.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // 6.- Llegir el fitxer "enters.txt" que trobaràs a la carpeta "fitxers" al Classroom i que mostri els números per
    //pantalla, digui quants números hi i ha i mostri també la suma de tots els números.
    private static void ex6() {
        try {
            System.out.println("Introdueix la ruta a l'arxiu a llegir: ");
            String nomArxiu = lector.nextLine();

            File arxiu = new File(nomArxiu);
            Scanner scanner = new Scanner(arxiu);

            int i = 0;
            int sum = 0;
            while (scanner.hasNext()) {
                int num = Integer.parseInt(scanner.next());
                System.out.print(num+" ");
                sum += num;
                i++;
            }
            System.out.println("\nTotal de números: "+i);
            System.out.println("Suma total: "+sum);
            scanner.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // 7.- Obtenir la línia més llarga i la més curta d'un fitxer de text.
    private static void ex7() {
        try {
            System.out.println("Introdueix la ruta a l'arxiu a llegir: ");
            String nomArxiu = lector.nextLine();

            Scanner sc = new Scanner(new File(nomArxiu));

            String largestLine;
            String shortestLine;
            // Añadimos cada linea en una lista.
            ArrayList<String> fileByLines = new ArrayList<>();
            while (sc.hasNextLine()) {
                fileByLines.add(sc.nextLine());
            }
            System.out.println(fileByLines);

            // Asignamos la linea con mas longitud y con menos de la lista
            largestLine = fileByLines.get(0);
            shortestLine = largestLine;
            for (int i = 1; i < fileByLines.size(); i++) {
                if (largestLine.length() < fileByLines.get(i).length())
                    largestLine = fileByLines.get(i);
                else if (shortestLine.length() > fileByLines.get(i).length())
                    shortestLine = fileByLines.get(i);
            }

            System.out.println(largestLine);
            System.out.println(shortestLine);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    // 8.- Eliminar el fitxer que s'introdueixi la ruta pel teclat.
    private static void ex8() throws IOException {
        Path fileDelPath = fitxersPath.resolve("fileDel.txt");
        File fileDel = new File(String.valueOf(fileDelPath));

        if (!fileDel.exists()) {
            Files.createFile(fileDelPath);
            System.out.printf("S'ha creat %s a %s\n", fileDel.getName(), fileDel.getParent());
        }

        System.out.println("Arxiu a esborrar:");
        String input = lector.nextLine();

        File inputFile = new File(input);

        if (inputFile.exists()) {
            if (inputFile.delete()) {
                System.out.println("S'ha esborrat: " + inputFile.getName());
            } else {
                System.out.println("Error al esborrar l'arxiu.");
            }
        }
    }

    // 9.- Que per qualsevol ruta entrada per teclat mostri:
    //✔ Si el fitxer existeix o no
    //✔ Si es tracta d'un directori o d'un fitxer
    //✔ Si és un fitxer, ha de mostrar les següents dades:
    //➢ Nom
    //➢ Mida
    //➢ Permisos de lectura i escriptura
    private static void ex9() throws IOException {
        System.out.println("Introdueix una ruta per mostrar la seva informació:");
        String ruta = lector.nextLine();
        Path path = Path.of(ruta);
        File f = new File(ruta);

        if (f.exists())
            System.out.println("L'arxiu existeix.");
        else
            System.out.println("L'arxiu no existeix.");

        if (f.isDirectory())
            System.out.println("L'arxiu és un directori");
        else if (f.isFile()) {
            System.out.println("L'arxiu és un fitxer.");
            BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
            System.out.println("Dades del fitxer:");
            System.out.println("> Nom: " + f.getName());
            System.out.printf("> Mida: %.0f KB\n", Math.ceil((float)attr.size() / 1024));
            System.out.print("> Permisos de lectura i escriptura: ");
            // Lectura
            if (f.canRead())
                System.out.print("r");
            else
                System.out.print("-");
            // Escriptura
            if (f.canWrite())
                System.out.print("w");
            else
                System.out.print("-");
//            // Execució
//            if (f.canExecute())
//                System.out.print("x");
//            else
//                System.out.print("-");
            System.out.println();
        }
    }

    // 10.- Que llegeixi el fitxer "restaurants.csv" que trobaràs a la carpeta "fitxers" al Classroom i mostri les dades
    //de tots els restaurants que es trobin a l'Eixample.
    private static void ex10() throws IOException {
        File rest = new File(String.valueOf(restaurantsPath));
        BufferedReader br = null;
        ArrayList<String> fileList = new ArrayList<>();
        try {

            br = new BufferedReader(new FileReader(rest));

            String line;
            while ((line = br.readLine()) != null) {
                fileList.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                br.close();
            }
        }

        fileList.forEach(line -> {
                    String[] parts = line.split(",");
                    if (parts[3].equals("Eixample"))
                        System.out.println(line);
                });
    }

    // 11.- Que permeti afegir per teclat nous restaurants al fitxer "restaurants.csv" utilitzant el mateix format que els
    //que ja hi són (Nom,Telefon,Direccio,Districte,.....).
    private static void ex11() throws IOException {
        File rest = new File(String.valueOf(restaurantsPath));
        FileWriter fw = new FileWriter(rest, true);
        BufferedWriter bw = new BufferedWriter(fw);

        // Nom,Telefon,Direccio,Districte,Barri,Ciutat,Codi Postal,Regio,Pais,Lat,Lon,Web,Mail
        String nom, dir, dist, barri, ciutat, regio, pais, web, mail;
        long tlf, cp;
        double lat, lon;

        System.out.println("Nom: ");
        nom = lector.nextLine();
        if (nom.equals(""))
            bw.write("-----,");
        else
            bw.write(nom+",");

        System.out.println("Telèfon: ");
        tlf = lector.nextInt();
        lector.nextLine();
        if (tlf == 0)
            bw.write("-,");
        else
            bw.write((int) tlf+",");

        System.out.println("Direcció: ");
        dir = lector.nextLine();
        if (dir.equals(""))
            bw.write("-----,");
        else
            bw.write(dir+",");

        System.out.println("Districte: ");
        dist = lector.nextLine();
        if (dist.equals(""))
            bw.write("-----,");
        else
            bw.write(dist+",");

        System.out.println("Barri: ");
        barri = lector.nextLine();
        if (barri.equals(""))
            bw.write("-----,");
        else
            bw.write(barri+",");

        System.out.println("Ciutat: ");
        ciutat = lector.nextLine();
        if (ciutat.equals(""))
            bw.write("-----,");
        else
            bw.write(ciutat.toUpperCase(Locale.ROOT)+",");

        System.out.println("Codi Postal: ");
        cp = lector.nextLong();
        lector.nextLine();
        if (cp == 0)
            bw.write("-----,");
        else
            bw.write(cp+",");

        System.out.println("Regió: ");
        regio = lector.nextLine();
        if (regio.equals(""))
            bw.write("-----,");
        else
            bw.write(regio+",");

        System.out.println("Pais: ");
        pais = lector.nextLine();
        if (pais.equals(""))
            bw.write("-----,");
        else
            bw.write(pais+",");

        System.out.println("Latitud: ");
        lat = lector.nextDouble();
        if (lat == 0)
            bw.write("-----,");
        else
            bw.write(lat+",");

        System.out.println("Longitud: ");
        lon = lector.nextDouble();
        lector.nextLine();
        if (lon == 0)
            bw.write("-----,");
        else
            bw.write(lon+",");

        System.out.println("Web: ");
        web = lector.nextLine();
        if (web.equals(""))
            bw.write("-----,");
        else
            bw.write(web+",");

        System.out.println("Mail: ");
        mail = lector.nextLine();
        if (mail.equals(""))
            bw.write("-----\n");
        else
            bw.write(mail+"\n");

        bw.close();
    }

    // 12.- Que faci una còpia del fitxer "restaurants.csv" que es digui “restaurants2.csv” que contingui les dades de
    //tots els restaurants que no estiguin a l'Eixample.
    private static void ex12() throws IOException {
        File rest = new File(String.valueOf(restaurantsPath));
        File restBackup = new File(String.valueOf(fitxersPath.resolve("restaurants2.csv")));

        if (!restBackup.exists())
            Files.createFile(Path.of(String.valueOf(restBackup)));

        BufferedReader br = null;
        ArrayList<String> fileList = new ArrayList<>();
        try {

            br = new BufferedReader(new FileReader(rest));

            String line;
            while ((line = br.readLine()) != null) {
                fileList.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                br.close();
            }
        }

        FileWriter fw = new FileWriter(restBackup, false);
        BufferedWriter bw = new BufferedWriter(fw);
        fileList.forEach(line -> {
                    String[] parts = line.split(",");
                    if (!(parts[3].equals("Eixample"))) {
                        try {
                            bw.write(line+"\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        bw.close();
    }

    // MAIN
    public static void main(String[] args) throws IOException {
        // Projecte
        System.out.println("Ruta projecte:");
        System.out.println(projectPath);

        // src
        System.out.println("Ruta src:");
        System.out.println(srcPath);

        // fitxers
        System.out.println("Ruta fitxers");
        System.out.println(fitxersPath);

        // fitxers > enters.txt
        System.out.println("> Enters:");
        System.out.println(entersPath);

        // fitxers > restaurants.csv
        System.out.println("> Restaurants:");
        System.out.println(restaurantsPath);

        // exercicis
        System.out.println("Ruta exercicis:");
        System.out.println(actPath);

        System.out.println();
//        ex1();
//        ex2();
//        ex3();
//        ex4();
//        ex5();
//        ex6();
//        ex7();
//        ex8();
//        ex9();
//        ex10();
        ex11();
//        ex12();
    }
}
